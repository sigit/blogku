from django.db import models


class Blog(models.Model):
    judul = models.CharField(max_length=200, blank=False)
    teks = models.TextField()
    tanggal = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.judul


# Selanjutnya jalankan command ini di terminal:
# python3 manage.py makemigrations
# python3 manage.py migrate
