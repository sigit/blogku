from django.shortcuts import render
from blogku.models import Blog


def list_blog(request):
    data = {
        "blogs": Blog.objects.all(),
    }
    return render(request, 'list_blog.html', data)


def detail_blog(request, blog_id):
    data = {
        'blog': Blog.objects.get(id=blog_id),
    }
    return render(request, 'detail_blog.html', data)


def halo(request):
    data = {
        'nama': "sigit",
        'waktu': request.GET.get('waktu', 'Pagi'),
    }
    return render(request, 'halo.html', data)
