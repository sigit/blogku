from django.contrib import admin
from blogku.models import Blog


class BlogAdmin(admin.ModelAdmin):
    list_display = ('judul', 'tanggal')
    list_filter = ('tanggal',)


admin.site.register(Blog, BlogAdmin)
