from django.http import JsonResponse
from django.conf import settings


def test(request):
    data = {
        'result': settings.VPN.test()
    }
    return JsonResponse(data)


def server_info(request):
    data = {
        'result': settings.VPN.server_info()
    }
    return JsonResponse(data)
